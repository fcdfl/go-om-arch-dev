package main

import (
	"fmt"
	"runtime"
)

func main() {
	fmt.Println("GOROOT:", runtime.GOROOT())
	fmt.Println("NumCPU:", runtime.NumCPU())
	fmt.Println("NumGoroutine:", runtime.NumGoroutine())
}
