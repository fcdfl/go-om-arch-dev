package main

import (
	"fmt"
	"sync"
)

func main() {
	var users sync.Map

	users.Store("001", "华安")
	users.Store("004", "华文")
	users.Store("009", "华武")
	users.Store("010", "秋香")

	users.Delete("010")

	for i := 1; i <= 10; i++ {
		key := fmt.Sprintf("%03d", i)
		value, ok := users.Load(key)
		fmt.Printf("%s: %v, %v\n", key, value, ok)
	}
}
