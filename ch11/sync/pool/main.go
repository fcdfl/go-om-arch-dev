package main

import (
	"log"
	"sync"
	"time"
)

type Client struct {
	Dsn     string
	Timeout time.Duration
}

func main() {
	pool := sync.Pool{New: func() interface{} {
		log.Println("Pool::New()")
		return &Client{
			Dsn:     "host=127.0.0.1 user=fcdfl port=5432",
			Timeout: 3 * time.Second,
		}
	}}

	// 第一次获取，因为池子为空，所以调用Pool::New创建一个对象
	cli := pool.Get()
	log.Printf("%#v, %p\n", cli, cli)

	// 将对象放回池中
	pool.Put(cli)
	// 因为池中有对象了，所以直接获取这个对象
	cli = pool.Get()
	log.Printf("%#v, %p\n", cli, cli)
}
