package main

import (
	"fmt"
	"sync"
	"time"
)

type NewFunc func() interface{}

type Pool struct {
	New  NewFunc
	objs []interface{}
	mut  sync.Mutex
}

func NewPool(size uint, newFunc NewFunc) *Pool {
	objs := make([]interface{}, size)
	if size > 0 {
		for i := uint(0); i < size; i++ {
			objs[i] = newFunc()
		}
	}
	return &Pool{
		New:  newFunc,
		objs: objs,
	}
}

func (p *Pool) IsEmpty() bool {
	return len(p.objs) < 1
}

func (p *Pool) Get() interface{} {
	p.mut.Lock()
	defer p.mut.Unlock()
	if p.IsEmpty() {
		return p.New()
	}
	obj := p.objs[0]
	p.objs = p.objs[1:]
	return obj
}

func (p *Pool) Put(obj interface{}) {
	p.mut.Lock()
	defer p.mut.Unlock()
	p.objs = append(p.objs, obj)
}

type Client struct {
	DSN     string
	Timeout time.Duration
}

func main() {
	p := NewPool(3, func() interface{} {
		fmt.Println("NewPool")
		return &Client{
			DSN:     "host=127.0.0.1 port=5432 user=fcdfl",
			Timeout: time.Second * 3,
		}
	})

	x := p.Get()
	fmt.Printf("%#v %p\n", x, x)
	p.Put(x)
	for i := 1; i < 10; i++ {
		x := p.Get()
		fmt.Printf("#%d, %#v %p\n", i, x, x)
	}
}
