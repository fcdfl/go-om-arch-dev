package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {
	var counter int
	wg := &sync.WaitGroup{}
	mut := &sync.Mutex{}

	incr := func() {
		defer wg.Done()
		for i := 0; i < 100; i++ {
			mut.Lock()
			counter++
			mut.Unlock()

			runtime.Gosched()
		}
	}
	decr := func() {
		defer wg.Done()
		for i := 0; i < 100; i++ {
			mut.Lock()
			counter--
			mut.Unlock()

			runtime.Gosched()
		}
	}

	for i := 0; i < 10; i++ {
		wg.Add(2)
		go incr()
		go decr()
	}
	wg.Wait()

	fmt.Println(counter)
}
