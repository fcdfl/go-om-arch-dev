package main

import "fmt"

func main() {
	//var notice chan string
	notice := make(chan string)
	fmt.Printf("%T, %v\n", notice, notice)

	go func() {
		fmt.Println("工作例程开始")
		notice <- "风从东方来"
		fmt.Println("工作例程结束")
	}()

	fmt.Println("主例程开始")
	fmt.Println("你好，", <-notice)
	fmt.Println("主例程结束")
}
