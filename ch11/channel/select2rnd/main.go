package main

import "fmt"

func main() {
	ch := make(chan int, 1)
	const num = 10
	s := make([]int, num)

	for i := 0; i < num; i++ {
		select {
		case ch <- 0:
		case ch <- 1:
		case ch <- 2:
		case ch <- 3:
		case ch <- 4:
		case ch <- 5:
		case ch <- 6:
		case ch <- 7:
		case ch <- 8:
		case ch <- 9:
		}
		s[i] = <-ch
	}

	fmt.Println(s)
}
