package main

import (
	"fmt"
	"time"
)

func main() {
	d := time.Second * 1

	ch1 := time.After(d)
	fmt.Println("After:", <-ch1) // After 只能读一次

	ch2 := time.Tick(d)
	for i := 0; i < 10; i++ {
		fmt.Printf("Tick #%d %v\n", i, <-ch2)
	}
}
