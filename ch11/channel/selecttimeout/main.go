package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	timeOut := time.Second * 3
	const maxSecs = 10

	ch := make(chan int)
	chTimeout := make(chan time.Time)

	now := time.Now()

	rand.Seed(now.UnixNano())

	go func() {
		rnd := rand.Intn(maxSecs)
		time.Sleep(time.Second * time.Duration(rnd))
		ch <- rnd
	}()
	go func() {
		time.Sleep(timeOut)
		chTimeout <- time.Now()
	}()

	select {
	case i := <-ch:
		fmt.Println("结果：", i)
		//case t := <-time.After(timeOut):
		//fmt.Println("超时：", t.Sub(now).Seconds())
	case t := <-chTimeout:
		fmt.Println("超时:", t.Sub(now).Seconds())
	}
}
