package main

import "fmt"

func main() {
	var ch = make(chan int, 5)
	var rch <-chan int = ch //只读
	var wch chan<- int = ch //只写

	// wch <- 123
	// fmt.Println(<-rch)
	// -- 错误示例
	// rch <- 123         // cannot send to receive-only channel of type <-chan int
	// fmt.Println(<-wch) // cannot receive from send-only channel of type chan<- int

	go func(ch <-chan int) {
		fmt.Println("goroutine:", <-ch)
	}(rch)

	go func(ch chan<- int) {
		ch <- 99
		close(ch)
	}(wch)

	for i := range ch {
		fmt.Println(i)
	}
}
