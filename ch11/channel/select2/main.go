package main

import (
	"fmt"
	"time"
)

func main() {
	ch1 := make(chan int, 1)
	ch2 := make(chan int, 1)

	ch1 <- 88
	ch2 <- 99

	go func() {
		fmt.Println("从ch1读取数据：", <-ch1)
	}()

	go func() {
		fmt.Println("从ch2读取数据：", <-ch2)
	}()

	select {
	case ch1 <- 1:
		fmt.Println("写入数据到ch1")
		close(ch1)
	case ch2 <- 2:
		fmt.Println("写入数据到ch2")
		close(ch2)
	case t := <-time.After(time.Second):
		fmt.Println("超时：", t)
	}
}
