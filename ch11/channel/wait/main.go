package main

import (
	"fmt"
	"runtime"
)

func main() {
	ch := make(chan struct{})
	const num = 100

	for i := 0; i < num; i++ {
		go printA2Z(i, ch)
	}

	for i := 0; i < num; i++ {
		<-ch
	}

	fmt.Println("\n结束")
}

func printA2Z(idx int, ch chan<- struct{}) {
	for i := 'A'; i <= 'Z'; i++ {
		fmt.Printf("[#%02d %c]\n", idx, i)
		runtime.Gosched()
	}
	ch <- struct{}{}
}
