package main

import (
	"log"
	"net"
	"time"
)

func main() {
	addr := ":9527"
	listener, err := net.Listen("tcp", addr)
	if err != nil {
		log.Fatal("Listen failed:", err)
	}
	defer listener.Close()
	log.Println("Listened on:", addr)

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Fatal("Accept failed:", err)
		}
		log.Println("Accepted new client:", conn.RemoteAddr().String())

		n, err := conn.Write([]byte(time.Now().Format("2006/01/02 15:04:05")))
		if err != nil {
			log.Fatal("Write failed:", err)
		}
		log.Println("Write data to client success, data size:", n)
		conn.Close()
	}

}
