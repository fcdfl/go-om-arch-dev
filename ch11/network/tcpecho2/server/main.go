package main

import (
	"bufio"
	"log"
	"net"

	"repo.or.cz/go-om-arch-dev/ch11/network/tcpecho2"
)

func main() {
	listen, err := net.Listen("tcp", tcpecho2.Addr)
	if err != nil {
		log.Fatal("Listen failed:", err)
	}
	defer listen.Close()

	for {
		conn, err := listen.Accept()
		if err != nil {
			log.Println("Accept failed:", err)
			break
		}
		log.Println("Accepted new client:", conn.RemoteAddr().String())
		go handler(conn)
	}
}

func handler(conn net.Conn) {
	defer conn.Close()
	reader := bufio.NewReader(conn)
	writer := bufio.NewWriter(conn)

	msg, err := reader.ReadString('\n')
	if err != nil {
		log.Println("Read from client failed:", err)
		return
	}
	if msg == "quit" {
		log.Println("客户端主动关闭连接")
		return
	}

	if _, err := writer.WriteString(msg + "\n"); err != nil {
		log.Println("Write to client failed:", err)
		return
	}
	writer.Flush()
}
