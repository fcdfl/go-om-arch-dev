package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"os"

	"repo.or.cz/go-om-arch-dev/ch11/network/tcpecho2"
)

func main() {
	conn, err := net.Dial("tcp", tcpecho2.Addr)
	if err != nil {
		log.Fatal("Dial to server failed:", err)
	}
	defer conn.Close()

	reader := bufio.NewReader(conn)
	writer := bufio.NewWriter(conn)
	input := bufio.NewScanner(os.Stdin)

	for {
		input.Scan()
		msg := input.Text()
		if _, err := writer.WriteString(msg + "\n"); err != nil {
			log.Println("Send to server failed:", err)
			continue
		}
		writer.Flush()

		msg, err := reader.ReadString('\n')
		if err != nil {
			log.Println("Read from server failed:", err)
			continue
		}
		fmt.Println(msg)
	}
}
