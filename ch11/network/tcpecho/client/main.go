package main

import (
	"fmt"
	"log"
	"net"
)

func main() {
	conn, err := net.Dial("tcp", "127.0.0.1:9527")
	if err != nil {
		log.Fatal("Dial to server failed:", err)
	}

	defer conn.Close()

	for {
		var msg string
		fmt.Println("请输入要发送的内容(`quit`退出)：")
		fmt.Scanln(&msg)

		if _, err := conn.Write([]byte(msg)); err != nil {
			log.Println("Write to server failed:", err)
			continue
		}

		if msg == "quit" {
			break
		}

		buf := make([]byte, 1024)
		n, err := conn.Read(buf)
		if err != nil {
			log.Println("Read from server failed:", err)
			continue
		}

		fmt.Println(string(buf[:n]))
	}
}
