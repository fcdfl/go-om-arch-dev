package main

import (
	"log"
	"net"
)

func main() {
	listen, err := net.Listen("tcp", "127.0.0.1:9527")
	if err != nil {
		log.Fatal("listen failed:", err)
	}
	defer listen.Close()

	for {
		conn, err := listen.Accept()
		if err != nil {
			log.Panicln("accept failed:", err)
			continue
		}

		for {
			buf := make([]byte, 1024)
			n, err := conn.Read(buf)
			if err != nil {
				log.Println("read from client failed:", err)
				continue
			}
			msg := buf[:n]
			if string(msg) == "quit" {
				log.Println("客户端主动断开连接")
				break
			}
			n, err = conn.Write(msg)
			if err != nil {
				log.Println("write to client failed:", err)
				continue
			}
		}
		conn.Close()
	}
}
