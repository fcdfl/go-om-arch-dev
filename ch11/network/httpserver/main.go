package main

import (
	"bufio"
	"bytes"
	"io"
	"io/ioutil"
	"log"
	"net"
	"strings"
)

func main() {
	listen, err := net.Listen("tcp", ":9527")
	if err != nil {
		log.Fatal(err)
	}
	defer listen.Close()

	log.Println("Server listen on:", listen.Addr())

	for {
		conn, err := listen.Accept()
		if err != nil {
			log.Println("Accept client failed:", err)
			continue
		}

		go handler(conn)

	}
}

func handler(conn net.Conn) {
	defer conn.Close()

	log.Println("Accepted new client:", conn.RemoteAddr())

	reader := bufio.NewReader(conn)
	// GET <PATH> HTTP/1.1
	request, err := reader.ReadString('\n') // 读取HTTP请求的第一行
	if err != nil {
		log.Println("Read HTTP request failed:", err)
		return
	}
	// 解析请求的URL
	fields := strings.Fields(request)
	method := fields[0]
	path := fields[1]
	version := fields[2]
	log.Printf("Request: method %q, path %q, version:  %q\n", method, path, version)
	// 响应
	response(path, conn)
}

func response(path string, writer io.Writer) {
	sb := bytes.Buffer{}
	switch path {
	case "/favicon.ico":
		make404Response(&sb)
	default:
		makeResponseFile("./main.go", &sb)
	}

	w := bufio.NewWriter(writer)
	w.Write(sb.Bytes())
	w.Flush()
}

func makeResponse(code int, content []byte, sb *bytes.Buffer) {
	sb.WriteString("HTTP/1.1 200\n")
	sb.WriteString("Content-type: text/plain; charset=utf-8\n")
	sb.WriteString("\r\n")
	sb.Write(content)
}

func make404Response(sb *bytes.Buffer) {
	makeResponse(404, []byte("没有找到"), sb)
}

func makeResponseFile(filename string, sb *bytes.Buffer) {
	content, err := ioutil.ReadFile("./main.go")
	if err != nil {
		make404Response(sb)
		return
	}
	makeResponse(200, content, sb)
}
