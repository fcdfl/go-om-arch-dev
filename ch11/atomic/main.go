package main

import (
	"fmt"
	"sync"
	"sync/atomic"
)

func main() {
	var counter int32

	wg := &sync.WaitGroup{}

	incr := func() {
		defer wg.Done()
		for i := 0; i < 100; i++ {
			atomic.AddInt32(&counter, 1)
		}
	}

	decr := func() {
		defer wg.Done()
		for i := 0; i < 100; i++ {
			atomic.AddInt32(&counter, -1)
		}
	}

	for i := 0; i < 10; i++ {
		wg.Add(2)
		go incr()
		go decr()
	}
	wg.Wait()
	fmt.Println(counter)
}
