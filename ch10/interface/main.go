package main

import "fmt"

type Sender interface {
	Send(to, msg string) error
	SendAll(tos []string, msg string) error
}

type EmailSender struct {
}

func (es *EmailSender) Send(to, msg string) error {
	return nil
}
func (es *EmailSender) SendAll(tos []string, msg string) error {
	for _, to := range tos {
		if err := es.Send(to, msg); err != nil {
			return err
		}
	}
	return nil
}

func main() {
	var sender Sender
	SenderInfo(sender)

	var emailSender Sender = &EmailSender{}
	SenderInfo(emailSender)
}

func SenderInfo(s Sender) error {
	fmt.Printf("%T, %#v\n", s, s)
	return nil
}
