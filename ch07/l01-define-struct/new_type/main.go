package main

import "fmt"

type Counter int // 自定义类型
type User map[string]string
type Callback func(...string)

func main() {
	c := Counter(123)
	c += 456
	fmt.Printf("%T, %v\n", c, c)

	u := make(User)
	u["name"] = "风从东方来"
	u["address"] = "地球"
	fmt.Printf("%#v\n", u)

	var f Callback = func(args ...string) {
		for i, v := range args {
			fmt.Println(i, v)
		}
	}

	f("foo", "bar")
}
