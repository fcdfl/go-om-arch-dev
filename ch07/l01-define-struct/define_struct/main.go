package main

import (
	"fmt"
	"time"
)

type User struct {
	ID       int
	Name     string
	Birthday time.Time
	Addr     string
	Phone    string
	Remark   string
}

func main() {
	var u User
	fmt.Printf("%#v\n", u)
}
