package main

import (
	"flag"
	"fmt"
)

func main() {
	port := flag.Int("p", 22, "端口号")

	host := flag.String("h", "localhost", "主机名")

	verbor := flag.Bool("v", false, "显示详情")

	help := flag.Bool("H", false, "帮助信息")

	flag.Parse()

	if *help {
		flag.Usage()
		return
	}
	fmt.Printf("主机：%s，端口：%d，显示详情？%v\n", *host, *port, *verbor)
	fmt.Println(flag.Args()) // 除了上面定义的flag之外的其它所有参数
}
