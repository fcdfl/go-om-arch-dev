package main

import (
	"flag"
	"fmt"
)

func main() {
	var port int
	flag.IntVar(&port, "p", 22, "端口号") // 绑定

	var host string
	flag.StringVar(&host, "h", "localhost", "主机名") // 绑定

	var verbor bool
	flag.BoolVar(&verbor, "v", false, "显示详情")

	var help bool
	// 默认是 `-h`，因为主机占用了，这里重新定义为 `-H` 显示帮助
	flag.BoolVar(&help, "H", false, "帮助信息")

	flag.Parse() // 解析

	/*
		// 自定义Usage
		flag.Usage = func() {
			fmt.Println("自定义的Usage")
			flag.PrintDefaults() // 默认的帮助信息
		}
	*/
	if help {
		flag.Usage() // 显示帮助信息
		return
	}
	fmt.Printf("主机：%s，端口：%d，显示详情？%v\n", host, port, verbor)
	fmt.Println(flag.Args()) // 除了上面定义的flag之外的其它所有参数
}
