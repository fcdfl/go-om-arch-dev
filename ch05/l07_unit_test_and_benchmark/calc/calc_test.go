package calc

import "testing"

func TestAdd(t *testing.T) {
	if 4 != Add(1, 3) {
		t.Fatal("1+3必须等于4")
	}
}

// go test -v -bench Fact
// go test -v -bench . -benmem
func BenchmarkFact(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Fact(i)
	}
}
