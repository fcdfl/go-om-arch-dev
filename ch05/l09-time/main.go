package main

import (
	"fmt"
	"time"
)

func main() {
	now := time.Now()
	fmt.Printf("%T, %v\n", now, now)

	// 2006-01-02 15:04:05
	fmt.Println(now.Format("2006/01/02 15:04:05"))
	fmt.Println(now.Format("2006年01月02日 15:04:05"))
	fmt.Println(now.Format("2006年01月02日"))
	fmt.Println(now.Format("15时04分05秒"))

	fmt.Println(now.Unix())
	fmt.Println(now.UnixNano())

	// 解析
	tstr := "2022-03-07"
	if t, err := time.Parse("2006-01-02", tstr); err != nil {
		fmt.Println("解析错误：", err)
		return
	} else {
		fmt.Println(t)
	}

	tstr = "2022-03-07 16:08:43"
	if t, err := time.Parse("2006-01-02 15:04:05", tstr); err != nil {
		fmt.Println("解析错误：", err)
		return
	} else {
		fmt.Println(t)
	}

	// 时间戳
	//t := time.Unix(0, 0)
	t := time.Unix(1646640709, 0)
	fmt.Println(t)

	// 时间差
	d := now.Sub(t)
	fmt.Printf("typeof d is %T, and value is: %v\n", d, d)

	fmt.Println(time.Now())
	// 休眠及时间区间常量
	time.Sleep(time.Second * 1)
	fmt.Println(time.Now())

	// 增加时间
	t = now.Add(time.Hour * 3)
	fmt.Println(t)

	// 解析时间区间
	if d, err := time.ParseDuration("3h2m1s"); err != nil {
		fmt.Println("解析错误：", err)
		return
	} else {
		fmt.Println("小时表示：", d.Hours(), "，分钟表示：", d.Minutes(), "，秒数表示：", d.Seconds(), "，字符表示：", d.String())
	}
}
