package main

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
)

func main() {
	fmt.Printf("%x\n", sha256.Sum256([]byte("风从东方来")))

	s := sha256.New()
	s.Write([]byte("风"))
	s.Write([]byte("从"))
	s.Write([]byte("东方"))
	s.Write([]byte("来"))
	h := s.Sum(nil)
	fmt.Println(hex.EncodeToString(h[:]))
}
