package main

import (
	"crypto/hmac"
	"crypto/sha256"
	"fmt"
)

func main() {
	key := []byte("DMGLfrCTEDHsGrvNnAmK")
	hash := hmac.New(sha256.New, key)
	//io.WriteString(hash, "风从东方来")
	hash.Write([]byte("风从东方来"))
	fmt.Printf("%x\n", hash.Sum(nil))
}
