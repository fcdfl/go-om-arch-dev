package main

import (
	"encoding/base64"
	"log"
)

func main() {
	// 编码
	str := "风从东方来"
	data := base64.StdEncoding.EncodeToString([]byte(str))
	log.Printf("%q\n", data)

	// 解码
	estr := "6aOO5LuO5Lic5pa55p2l"
	if data, err := base64.StdEncoding.DecodeString(estr); err != nil {
		log.Fatal(err)
	} else {
		log.Printf("%q\n", data)
	}
}
