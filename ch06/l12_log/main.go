package main

import "log"

func main() {
	log.SetPrefix("[ FOO ] ") // 设置前缀
	log.Println("你好，世界")

	log.SetPrefix("[ BAR ] ")
	log.SetFlags(log.LstdFlags | log.Lshortfile) // 设置格式
	log.Println("风从东方来")
}
