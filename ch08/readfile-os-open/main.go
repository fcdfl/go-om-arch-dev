package main

import (
	"fmt"
	"io"
	"log"
	"os"
)

func main() {
	path := "main.go"
	file, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	buf := make([]byte, 10)
	for {
		n, err := file.Read(buf)
		if err != nil {
			if err != io.EOF {
				log.Println("读取错误：", err)
			}
			break
		}
		fmt.Print(string(buf[:n]))
	}
}
