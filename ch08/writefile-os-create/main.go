package main

import (
	"log"
	"os"
)

func main() {
	path := "/tmp/fcdfl.txt"
	file, err := os.Create(path)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	if n, err := file.Write([]byte("风从东方来\n")); err != nil {
		log.Fatal("写入错误：", err)
	} else {
		log.Println("成功写入", n, "字节数据")
	}

	if n, err := file.WriteString("啦狂散热啦框架法让牢记啊扽"); err != nil {
		log.Fatal("写入错误：", err)
	} else {
		log.Println("成功写入", n, "字节数据")
	}

	file.Sync()
}
