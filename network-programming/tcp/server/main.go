package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net"
	"strings"

	"repo.or.cz/go-om-arch-dev/network-programming/tcp"
)

func main() {
	listen, err := net.Listen("tcp", tcp.ServerAddr)
	if err != nil {
		log.Fatal(err)
	}
	defer listen.Close()

	log.Println("Server running", tcp.ServerAddr)

	for {
		conn, err := listen.Accept()
		if err != nil {
			log.Println("Accept failed", err)
			continue
		}
		log.Println("New Client:", conn.RemoteAddr())
		go handler(conn)
	}
}

func handler(conn net.Conn) {
	defer conn.Close()

	reader := bufio.NewReader(conn)
	writer := bufio.NewWriter(conn)
	for {
		msg, err := reader.ReadString('\n')
		if err == io.EOF {
			log.Println("用户关闭连接")
			break
		}
		if err != nil {
			log.Println("Read from client failed:", err)
			break
		}
		msg = strings.TrimSpace(msg)
		log.Println("客户发送的数据：", msg)
		msg = fmt.Sprintf("[ECHO] %s\n", msg)
		if _, err := writer.WriteString(msg); err != nil {
			log.Println("Write to client failed:", err)
			break
		}
		writer.Flush()
	}
}
