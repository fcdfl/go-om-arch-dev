package main

import (
	"bufio"
	"encoding/base64"
	"io"
	"log"
	"net"
	"os/exec"
	"strings"
)

func main() {
	listen, err := net.Listen("tcp", ":9981")
	if err != nil {
		log.Fatal(err)
	}
	defer listen.Close()

	log.Println("Server listen:", listen.Addr())

	for {
		conn, err := listen.Accept()
		if err != nil {
			log.Println("Accept failed:", err)
			continue
		}
		log.Println("Welcome new client:", conn.RemoteAddr())

		go handler(conn)
	}
}

func handler(conn net.Conn) {
	defer conn.Close()

	r := bufio.NewReader(conn)
	w := bufio.NewWriter(conn)
	for {
		msg, err := r.ReadString('\n')
		if err == io.EOF {
			log.Println("Disconnected by client")
			return
		}
		if err != nil {
			log.Println("Recived client data failed:", err)
			continue
		}
		msg = strings.TrimSpace(msg)
		sb := strings.Builder{}
		switch msg {
		case "ls":
			out, err := shellExec("ls", "-lh", "/tmp")
			if err != nil {
				log.Println("ls failed:", err)
				continue
			}
			sb.Write(out)
		case "cat":
			out, err := shellExec("cat", "./main.go")
			if err != nil {
				log.Println("cat failed:", err)
				continue
			}
			sb.Write(out)
		default:
			sb.WriteString("Unknow command: ")
			sb.WriteString(msg)
		}

		if _, err := w.WriteString(base64.StdEncoding.EncodeToString([]byte(sb.String())) + "\n"); err != nil {
			log.Println("Send data to client failed:", err)
			continue
		}
		w.Flush()
	}
}

func shellExec(c string, args ...string) ([]byte, error) {
	return exec.Command(c, args...).Output()
}
